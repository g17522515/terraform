terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      
      version = "4.27.0"
    }
  }
}


provider "aws" {
  region  = "eu-central-1"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
  #token = var.aws_session_token
  
}

# Create an S3 bucket
resource "aws_s3_bucket" "example_bucket70767" {
  bucket = "example-bucket-707677899"
  
}

# Create a security group
resource "aws_security_group" "example_sg" {
  name_prefix = "example_sg"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}





# Create an EC2 instance
resource "aws_instance" "example_instance" {
  ami           = "ami-0b7fd829e7758b06d"
  instance_type = "t2.micro"
  key_name      = "euncentral"
  vpc_security_group_ids = [aws_security_group.example_sg.id]


  tags = {
    Name = "example_instance"
  }
}
  


